// @name           Youtube Downloader
// @namespace      http://creazy.net/
// @description    Add downloadable links in Youtube Page (Boomarklet ver)
// @include        http://*youtube.com/watch*

//&fmt=22： HD   /MP4/H.264/AAC : 22/2000000/9/0/115
//&fmt=35： HQ   /FLV/H.264/AAC : 35/640000/9/0/115
//&fmt=18： iPod /MP4/H.264/AAC : 18/512000/9/0/115
//Normal ： LQ   /FLV/H.263/MP3

// == Bookmarklet ==
// javascript:var d=document,s=d.createElement('script');s.charset='UTF-8';s.src='http://labs.creazy.net/bookmarklet/youtube_downloader_max.js';d.body.appendChild(s);void(0);

(function() {

    var d = document;
    var h = '';
    var w = window;
    var l = location;
    var s = w.swfArgs;
    var u = 'http://'+l.host+'/get_video' + '?video_id='+s['video_id'] + '&t=' + s['t'];

    var formats = [
        [40,'40'],
        [39,'39'],
        [38,'38'],
        [37,'37'],
        [36,'36'],
        [35,'HQ/FLV/H.264/AAC'],
        [34,'34'],
        [33,'33'],
        [32,'32'],
        [31,'31'],
        [30,'30'],
        [29,'29'],
        [28,'28'],
        [27,'27'],
        [26,'26'],
        [25,'25'],
        [24,'24'],
        [23,'23'],
        [22,'HD/MP4/H.264/AAC'],
        [21,'21'],
        [20,'20'],
        [19,'19'],
        [18,'18'],
        [17,'17'],
        [16,'16'],
        [15,'15'],
        [14,'14'],
        [13,'13'],
        [12,'12'],
        [11,'11'],
        [10,'10'],
        [9,'9'],
        [8,'8'],
        [7,'7'],
        [6,'6'],
        [5,'5'],
        [4,'4'],
        [3,'3'],
        [2,'2'],
        [1,'1'],
        [0,'0']
    ];

    if( !d.getElementById('DL-YT-video') ) {
        // Create Links Block
        divElement = d.createElement('div');
        divElement.setAttribute('id','DL-YT-video');
        d.getElementById('watch-embed-div').appendChild(divElement);
    }
    // Create Links Block
    for ( i=0; i<formats.length; i++ ) {
        d.getElementById('DL-YT-video').innerHTML += '<div id="check_fmt_'+formats[i][0]+'">checking fmt='+formats[i][0]+'</div>';
    }
    d.getElementById('DL-YT-video').innerHTML
        += '<div><a href="'+makeDownloadURL(18)+'">[OK] DL fmt=18 ( iPod /MP4/H.264/AAC)</a></div>'
        +  '<div><a href="'+u+'">[OK] DL normal ( LQ /FLV/H.263/MP3)</a></div>';
    checkHD();
    checkHQ();
    for ( i=0; i<formats.length; i++ ) {
        checkFormat(formats[i][0]);
    }

    /**
     * create XmlHttpRequest
     */
    function createXHR() {
        if ( w.ActiveXObject ) {
            try {
                return new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    return new ActiveXObject("Microsoft.XMLHTTP");
                } catch (e2) {
                    return null;
                }
            }
        } else if ( w.XMLHttpRequest ) {
            return new XMLHttpRequest();
        } else {
            return null;
        }
    }

    /**
     * check HQ(fmt=35)
     */
    function checkFormat(id) {
        var url = 'http://'+location.host+'/watch' + '?fmt='+id+'&v='+s['video_id'];
        var XHR = createXHR();
//        XHR.testurl = url;
        XHR.open( 'GET', url, true );
        XHR.onreadystatechange = function() { 
            if (XHR.readyState==4) {
                if ( match = XHR.responseText.match(/var swfArgs = ({.*})/) ) {
w.console.log(id);
                    var json = eval('('+RegExp.$1+')');
                    var map = decodeURIComponent(json['fmt_url_map']);
                    var maps = map.split(',');
w.console.log(maps);
                    for ( i=0; i<maps.length; i++ ) {
                        fid = maps[i].split('|')[0];
w.console.log('fid='+fid);
                        if ( d.getElementById('check_fmt_'+fid) ) {
                            ftx = '';
                            for ( j=0; j<formats.length; j++ ) {
                                if ( formats[j][0] == fid ) ftx = formats[j][1];
                                break;
                            }
                            d.getElementById('check_fmt_'+fid).innerHTML = '<a href="'+u+'&fmt='+fid+'">[OK] DL fmt='+fid+' ('+ftx+')</a>';
                        } else {
w.console.log('not exists : check_fmt_'+fid);
                        }
                    }
                }
            }
        }
        XHR.send('');
    }

    /**
     * check HD(fmt=22)
     */
    function checkHD() {
        var url = 'http://'+location.host+'/watch' + '?v='+s['video_id'];
        var XHR = createXHR();
//        XHR.testurl = url;
        XHR.open( 'GET', url, true );
        XHR.onreadystatechange = function() { 
            if (XHR.readyState==4) {
                if ( match = XHR.responseText.match(/var swfArgs = ({.*})/) ) {
                    var block = d.getElementById('check_fmt_22');
                    block.innerHTML = '';
                    var json = eval('('+RegExp.$1+')');
                    var map = decodeURIComponent(json['fmt_url_map']);
                    if ( map.indexOf('22|') > -1 ) {
                        block.innerHTML
                            += '<a href="'+makeDownloadURL(22)+'">[OK] DL fmt=22 ( HD /MP4/H.264/AAC)</a>';
                    } else {
                        block.innerHTML
                            += '[NG] DL fmt=22 ( HD /MP4/H.264/AAC)';
                    }
                }
            }
        }
        XHR.send('');
    }

    /**
     * check HQ(fmt=35)
     */
    function checkHQ() {
        var url = 'http://'+location.host+'/watch' + '?fmt=35&v='+s['video_id'];
        var XHR = createXHR();
//        XHR.testurl = url;
        XHR.open( 'GET', url, true );
        XHR.onreadystatechange = function() { 
            if (XHR.readyState==4) {
                if ( match = XHR.responseText.match(/var swfArgs = ({.*})/) ) {
                    var block = d.getElementById('check_fmt_35');
                    block.innerHTML = '';
                    var json = eval('('+RegExp.$1+')');
                    var map = decodeURIComponent(json['fmt_url_map']);
                    if ( map.indexOf('35|') > -1 ) {
                        block.innerHTML
                            += '<a href="'+makeDownloadURL(35)+'">[OK] DL fmt=35 ( HQ /FLV/H.264/AAC)</a>';
                    } else {
                        block.innerHTML
                            += '[NG] DL fmt=35 ( HQ /FLV/H.264/AAC)';
                    }
                }
            }
        }
        XHR.send('');
    }

    /**
     * Make Download URL
     */
    function makeDownloadURL(num) {
        return u+'&fmt='+num;
    }

})();