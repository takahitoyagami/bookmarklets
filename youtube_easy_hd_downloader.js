// Youtube Easy HD Downloader

var d = document;
var w = window;

function createXHR() {
    if ( w.ActiveXObject ) {
        try {
            return new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                return new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e2) {
                return null;
            }
        }
    } else if ( w.XMLHttpRequest ) {
        return new XMLHttpRequest();
    } else {
        return null;
    }
}

checkURL(swfArgs['video_id']);

/**
 * Checking
 */
function checkURL(video_id) {
    var url
        = 'http://'+location.host+'/watch'
        + '?v='+video_id
        + '&fmt=22';
    var XHR = createXHR();
    XHR.open( 'GET', url, true );
    XHR.onreadystatechange = function() { 
        if (XHR.readyState==4) {
            if ( match = XHR.responseText.match(/var swfArgs = ({.*})/) ) {
                json = eval('('+RegExp.$1+')');
                if ( json['fmt_map'] == '22/2000000/9/0/115' ) {
                    if ( confirm("Found HD file! Download it?") ) {
                        location.href = '/get_video?video_id='+swfArgs['video_id']+'&t='+swfArgs['t']+'&fmt=22';
                    }
                } else {
                    if ( confirm("Can't find HD file! Download MP4 HQ?") ) {
                        location.href = '/get_video?video_id='+swfArgs['video_id']+'&t='+swfArgs['t']+'&fmt=18';
                    }
                }
            }
        }
    }
    XHR.send('');
}
