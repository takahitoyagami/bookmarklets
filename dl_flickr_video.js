function __create_http_request(){
	if(window.ActiveXObject){
		try {
			return new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				return new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e2) {
				return null;
			}
		}
	} else if(window.XMLHttpRequest){
		return new XMLHttpRequest();
	} else {
		return null;
	}
}

__node_ids = document.getElementById('thetags').innerHTML.match(/tagdiv([0-9]+\-[0-9]+)/g);
__node_id  = __node_ids[0].substring(6);

var __httpobj = __create_http_request();
__httpobj.open( 'get' , 'http://www.flickr.com/video_playlist.gne?node_id='+__node_id+'&secret='+page_p.secret , true );
__httpobj.onreadystatechange = function() { 
	if ( __httpobj.readyState == 4 ) { 
		xml  = __httpobj.responseXML;
		var __node =
			xml.getElementsByTagName('DATA')[0]
				.getElementsByTagName('SEQUENCE-ITEM')[0]
					.getElementsByTagName('STREAM')[0];
		location.href = __node.getAttribute('APP') + __node.getAttribute('FULLPATH');
	}
}

__httpobj.send();
