// ==UserScript==
// @name           Link Anyware
// @namespace      http://creazy.net/
// @description    URL and Title Copy Utility
// @include        http://www.google.*/search*
// ==/UserScript==

(function() {

	var w = window;
	var d = document;
	var t = d.title;
	var u = w.location;
	var s = '';
	if ( d.selection ) {
		s = d.selection.createRange().text;
	} else if ( w.selection ) {
		s = w.selection.createRange().text;
	} else if ( d.getSelection ) {
		s = d.getSelection();
	} else if ( w.getSelection ) {
		s = w.getSelection();
	}

	_URL_    = __escapeAmp(u);
	_TITLE_  = __escapeAmp(t);
	_SELECT_ = __escapeAmp(s.replace(/\n/g,"<br />"));

	//------------------------------------------------------------
	// Functions
	//------------------------------------------------------------

	/**
	 * Add a new link template
	 */
	var formats = [];
	function __addLinkTemplate( name, template ) {
		formats[formats.length] = { "name":name, "template":template};
	}

	/**
	 * HTML escape
	 */
	function __escapeHTML( str ) {
		str = str.replace(/"/g,"&quot;"); // "
		str = str.replace(/</g,"&lt;");   // <
		str = str.replace(/>/g,"&gt;");   // >
		return str;
	}

	/**
	 * & escape
	 */
	function __escapeAmp( str ) {
		str = str.replace(/&/g,"&amp;");  // &
		return str;
	}

	/**
	 * open Link Block
	 */
	function __openLinkBlock() {
		out = '<p><input type="button" value="X" onclick="__closeLinkBlock();" /></p>';
		out += '<dl>';
		for ( i=0; i<formats.length; i++ ) {
			out += '<dt>'+formats[i].name+'<dt>';
			out += '<dd>'+formats[i].template+'<dd>';
			out += '<dd>';
			out += '<input type="text" id="__copy_link_'+i+'" value="'+__escapeHTML(formats[i].template)+'" onfocus="this.select();" tabindex="'+(100+i)+'" />';
			out += '<dd>';
		}
		out += '</dl>';
		if ( !d.getElementById('__copy_link_block__') ) {
			bobj = d.createElement('div');
			bobj.setAttribute('id','__copy_link_block__');
			bobj.style.position        = 'absolute';
			bobj.style.top             = '0';
			bobj.style.left            = '0';
			bobj.style.color           = '#000000';
			bobj.style.backgroundColor = '#FFFFFF';
			bobj.style.textAlign       = 'left';
			bobj.innerHTML = out;
		}
		d.body.appendChild(bobj);
		d.getElementById('__copy_link_0').focus();
	}
	/**
	 * close Link Block
	 */
	function __closeLinkBlock() {
		d.body.removeChild(d.getElementById('__copy_link_block__'));
	}

	//------------------------------------------------------------
	// Templates
	//------------------------------------------------------------
	__addLinkTemplate(
		'Copy Title Link',
		'<a href="'+_URL_+'" target="_blank">'+_TITLE_+'</a>'
	);
	__addLinkTemplate(
		'Copy URL Link',
		'<a href="'+_URL_+'" target="_blank">'+_URL_+'</a>'
	);
	__addLinkTemplate(
		'Copy Bloclquote',
		'<cite>via: <a href="'+_URL_+'" target="_blank">' +_TITLE_+'</a></cite>'
		+'<blockquote cite="'+_URL_+'" title="'+_TITLE_+'">'
		+'<p>'+_SELECT_+'</p>'
		+'</blockquote>'
	);
	__addLinkTemplate(
		'Copy Title + Hatebu Link',
		'<a href="'+_URL_+'" target="_blank">'+_TITLE_+'</a>'
		+'<a href="http://b.hatena.ne.jp/entry/'+_URL_+'" target="_blank">'
		+'<img src="http://b.hatena.ne.jp/entry/image/small/'+_URL_+'" alt="" border="0" />'
		+'</a>'
	);
	__addLinkTemplate(
		'Copy Screenshot(HeartRails Capture) Link',
		'<a href="'+_URL_+'" target="_blank"><img title="'+_TITLE_+'" src="http://capture.heartrails.com/small?'+_URL_+'" alt="'+_URL_+'" width="120" height="90" border="0" /></a>'
	);

	__openLinkBlock();

})();
