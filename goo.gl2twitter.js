/**
 * goo.gl2twitter
 * 
 * Usage:
 * javascript:(function(){window.open('about:blank','twitter_status');var d=document,s=d.createElement('script');s.type='text/javascript';s.src="http://labs.creazy.net/bookmarklet/goo.gl2twitter.js";d.body.appendChild(s);})();
 * @see http://ggl-shortener.appspot.com/instructions/
 */

(function(){

    window.tweetGoogleURLShortenerCallback = function(json) {
        var url    = 'http://twitter.com/home?status=';
        var status = document.title + ' ' + json.short_url + ' #webclip';
        window.open(url+encodeURIComponent(status),'twitter_status');
    }
    
    var api = 'http://ggl-shortener.appspot.com/?jsonp=tweetGoogleURLShortenerCallback&url=';
    
    var script = document.createElement('script');
    script.type    = 'text/javascript';
    script.charset = 'UTF-8';
    script.src     = api + encodeURIComponent(location.href);
    document.body.appendChild(script);

})();