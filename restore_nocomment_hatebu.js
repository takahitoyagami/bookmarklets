var bookmarked_user      = document.getElementById('bookmarked_user');
var bookmarked_entry_url = document.getElementById('entrylink_url').firstChild.href;
var bookmark_offset      = 0;

bookmarked_user.innerHTML = '';
requestHatebuRSS();

function requestHatebuRSS() {
	var global = function() { return this }();
	global.__callbackHatebuRSS__ = renderHatebuRSS;

	var pipes_url
		= 'http://pipes.yahoo.com/yager/hatebu_bookmarklist'
		+ '?_render=json'
		+ '&_callback=__callbackHatebuRSS__'
		+ '&url='+encodeURIComponent(bookmarked_entry_url.replace(/#/,'%23'))
		+ '&of='+bookmark_offset;
	var s = document.createElement('script');
	s.src = pipes_url;
	document.body.appendChild(s);
}

function renderHatebuRSS(json)
{
	var items = json.value.items;
	for ( var i=0; i<items.length; i++ ) {
		author  = items[i]['dc:creator'];
		dc_date = items[i]['dc:date'];
		sbm_url = items[i]['rdf:about'];
		tags    = items[i]['dc:subject'];
		comment = items[i]['description'];

		if ( items[i]['annotate:reference']['rdf:resource'] != bookmarked_entry_url ) continue;
		if ( document.getElementById('bookmark-user-'+author) ) continue;

		dc_date = dc_date.replace(/^(\d{4}-\d{2}-\d{2})T(\d{2}:\d{2}).*/,"$1 $2");
		icon_url = 'http://www.hatena.ne.jp/users/'+author.substring(0,2)+'/'+author+'/profile_s.gif';
		li=document.createElement('li');
		li.setAttribute('id','bookmark-user-'+author);
		li_html
			= '<span class="timestamp">'+dc_date+'</span> '
			+ '<a href="/'+author+'/" class="hatena-id-icon">'
			+ '<img src="'+icon_url+'" class="hatena-id-icon" alt="'+author+'" title="'+author+'" height="16" width="16"></a> '
			+ '<a href="'+sbm_url+'" class="hatena-id">'+author+'</a> ';
		if ( tags ) {
			if ( typeof(tags) == 'string' ) {
				tags = new Array(tags);
			}
			li_html += '<span class="user-tag">';
			for ( var j=0; j<tags.length; j++ ) {
				if ( j>0 ) li_html += ', ';
				li_html
					+= '<a href="/'+author+'/'+encodeURIComponent(tags[j])+'/" rel="tag" class="user-tag">'
					+  tags[j]+'</a>';
			}
			li_html += '</span> ';
		}
		if ( comment ) {
			li_html += '<span class="comment">'+comment+'</span>';
		}
		li.innerHTML += li_html;
		bookmarked_user.appendChild(li);
	}
	if ( items.length > 0 ) {
		bookmark_offset += items.length;
		requestHatebuRSS();
	} else {
		new Hatena.Star.EntryLoader();
	}
}
