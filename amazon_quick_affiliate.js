// ==UserScript==
// @name           Amazon Quick Affiliate (JP)
// @namespace      http://creazy.net/
// @description    Display Amazon Associate Affiliate forms for ease in copy and past to your Blog
// @include        http://www.amazon.co.jp/*
// ==/UserScript==

(function() {

    //------------------------------------------------------------
    var d  = document;
    var w = (typeof(unsafeWindow) != 'undefined') ? unsafeWindow : window;
    var l = location;

    var url_base = 'http://www.amazon.co.jp/exec/obidos/ASIN/';
    var img_base = 'http://ecx.images-amazon.com/images/I/';
    var affiliate_images   = [];
    
    // Product Information
    var asin     = '';
    var title    = '';
    var url      = '';
    var img      = '';
    var imgid    = '';

    // User Settings (default)
    var affiliate_id       = 'creazynet-22';
    var affiliate_enable   = 0;
    var affiliate_template
        = '<a href="${url}" target="_blank">${title}</a><br />'
        + '<a href="${url}" target="_blank"><img src="${img}" alt="${asin}" border="0" /></a>';
    //------------------------------------------------------------

    /**
     * Add cookie
     */
    function addCookie(key, value) {
        if ( !key || !value ) {
            return false;
        }
        d.cookie
            = key + '=' + escape(value) + '; '
            + 'expires=Tue, 1-Jan-2030 00:00:00 GMT; '
            + 'path=/; ';
    }
    /**
     * Get cookie
     */
    function getCookie(key) {
        var cookies = d.cookie.split(';');
        for ( var i=0; i<cookies.length; i++ ) {
            if ( cookies[i].indexOf('=') < 0 ) continue;
            key_value  = cookies[i].replace(/(^[\s]+|;)/g,'');
            deli_index = key_value.indexOf('=');
            if ( key_value.substring(0,deli_index) == key ) {
                return unescape(key_value.substring(deli_index+1,key_value.length));
            }
        }
        return '';
    }
    /**
     * Save Settings to cookie
     */
    function saveSettings() {
        addCookie('affiliate_id',      d.getElementById('affiliate_id').value);
        addCookie('affiliate_enable',  d.getElementById('affiliate_enable').checked?'1':'0');
        addCookie('affiliate_template',d.getElementById('affiliate_template').value);
        alert('Saved');
        l.reload(true);
    }

    /**
     * escape HTML specialchars
     */
    function esc(str) {
        str = str.replace(/&/g,'&amp;');   // &
        str = str.replace(/</g,'&lt;');    // <
        str = str.replace(/>/g,'&gt;');    // >
        str = str.replace(/\"/g,'&quot;'); // "
        str = str.replace(/\'/g,'&#039;'); // '
        return str;
    }

    /**
     * Set Affiliate format
     */
    function setAffiliateFormat() {
        var radios = d.getElementById('affiliate_img_sizes').getElementsByTagName('input');
        var imgid = '';
        var imgno = '';
        for ( var i=0; i<radios.length; i++ ) {
            if ( radios[i].checked ) {
                imgid = radios[i].getAttribute('class');
                imgno = radios[i].getAttribute('value').replace('px','');
                break;
            }
        }
        var aid   = document.getElementById('affiliate_aid').value;
        var asin  = document.getElementById('affiliate_asin').value;
        var title = document.getElementById('affiliate_title').value;
        var url   = document.getElementById('affiliate_url').value;
        var img   = img_base + imgid + '._SL' + imgno + '_.jpg';

        var custom_tag = document.getElementById('affiliate_template').value;
        custom_tag = custom_tag.replace(/\${aid}/g,   aid);
        custom_tag = custom_tag.replace(/\${asin}/g,  asin);
        custom_tag = custom_tag.replace(/\${title}/g, title);
        custom_tag = custom_tag.replace(/\${url}/g,   url);
        custom_tag = custom_tag.replace(/\${img}/g,   img);
        custom_tag = custom_tag.replace(/\${imgid}/g, imgid);

        // Text Link
        document.getElementById('affiliate_text_link').innerHTML
            = '<a href="'+url+'" target="_blank">'+title+'</a>';
        document.getElementById('affiliate_text_tag').value
            = '<a href="'+url+'" target="_blank">'+title+'</a>';

        // Image Link
        document.getElementById('affiliate_image_link').innerHTML
            = '<a href="'+url+'" target="_blank"><img src="'+img+'" alt="'+asin+'" border="0" /></a>';
        document.getElementById('affiliate_image_tag').value
            = '<a href="'+url+'" target="_blank"><img src="'+img+'" alt="'+asin+'" border="0" /></a>';

        // Custom Link
        document.getElementById('affiliate_custom_link').innerHTML
            = custom_tag;
        document.getElementById('affiliate_custom_tag').value
            = custom_tag;
    }

    /**
     * Initialize
     */
    function init() {
        // Loading User Settings from COOKIE
        affiliate_id       = (getCookie('affiliate_id')!=='')       ? getCookie('affiliate_id')       : affiliate_id;
        affiliate_enable   = (getCookie('affiliate_enable')!=='')   ? getCookie('affiliate_enable')   : affiliate_enable;
        affiliate_template = (getCookie('affiliate_template')!=='') ? getCookie('affiliate_template') : affiliate_template;

        // Affiliate values
        asin  = d.getElementById('ASIN').value;
        title = d.getElementById('btAsinTitle').innerHTML;
        if ( d.getElementById('prodImageCell') &&
             d.getElementById('prodImageCell').getElementsByTagName('img').length > 0 &&
             d.getElementById('prodImageCell').getElementsByTagName('img')[0].src.match(/http:\/\/(.+?)\/images\/I\/([^\.]+)\..+/) ) {
            img_base = 'http://' + RegExp.$1 + '/images/I/';
            imgid    = RegExp.$2;
            img      = img_base + imgid + '._SL160_.jpg';
            affiliate_images[affiliate_images.length] = imgid;
        }
        url   = url_base + asin + '/' + (affiliate_id?affiliate_id+'/':'');

        // "Alternative" images
        var original_img = d.getElementById('original_image');
        var image_tds    = (original_img) ? original_img.parentNode.getElementsByTagName('td') : [];
        if ( image_tds.length > 0 ) {
            for ( var i=0; i<image_tds.length; i++ ) {
                if ( image_tds[i].getAttribute('id') && image_tds[i].getAttribute('id').match(/(alt_image_[0-9]+)/) ) {
                    image_obj = image_tds[i].getElementsByTagName('img')[0];
                    // Get image ID
                    if ( image_obj.src.match(/http:\/\/(.+?)\/images\/I\/([^\.]+)\..+/) ) {
                        affiliate_images[affiliate_images.length] = RegExp.$2;
                    }
                }
            }
        }
        buildHTML();
    }

    /**
     * Build HTML
     */
    function buildHTML() {
        // build Image Sizes controller
        var affiliate_images_html = '';
        if ( affiliate_images.length > 0 ) {
            affiliate_images_html = '<table id="affiliate_img_sizes" cellpadding="0" cellspacing="0" border="0">';
            for ( var i=0; i<affiliate_images.length; i++ ) {
                imgid = affiliate_images[i];
                affiliate_images_html
                    += '<tr>'
                    +  '<td><img src="' + img_base + imgid + '._SL30_.jpg" alt="' + imgid + '" border="0" /></td>'
                    +  '<td><input type="radio" name="affiliate_image" id="'+imgid+'_30" class="'+imgid+'" value="30px" /><label for="'+imgid+'_30">30px</label></td>'
                    +  '<td><input type="radio" name="affiliate_image" id="'+imgid+'_75" class="'+imgid+'" value="75px" /><label for="'+imgid+'_75">75px</label></td>'
                    +  '<td><input type="radio" name="affiliate_image" id="'+imgid+'_160" class="'+imgid+'" value="160px"'+ (i==0?' checked="checked"':'') +' /><label for="'+imgid+'_160">160px</label></td>'
                    +  '<td><input type="radio" name="affiliate_image" id="'+imgid+'_500" class="'+imgid+'" value="500px" /><label for="'+imgid+'_500">500px</label></td>'
                    +  '</tr>';
            }
            affiliate_images_html += '</table>';
        }

        // build Affiliate block
        var block = d.createElement('div');
        block.innerHTML
            = '<input type="hidden" id="affiliate_aid" value="' + esc(affiliate_id) + '" />'
            + '<input type="hidden" id="affiliate_asin" value="' + esc(asin) + '" />'
            + '<input type="hidden" id="affiliate_title" value="' + esc(title) + '" />'
            + '<input type="hidden" id="affiliate_url" value="' + esc(url) + '" />'
            + '<input type="hidden" id="affiliate_img" value="' + esc(img) + '" />'
            + '<input type="hidden" id="affiliate_imgid" value="' + esc(imgid) + '" />'
            /* Header */
            + '<h2>'
            + '<a id="af_switch_on" href="javascript:void(0);">[open]</a>'
            + '<a id="af_switch_off" href="javascript:void(0);">[close]</a>'
            + ' Quick Affiliate</h2>'
            + ' Short URL → http://www.amazon.co.jp/dp/' + asin + '/<br />'
            /* Affiliate Block */
            + '<div id="affiliate_block">'
            + '<table cellpadding="5" cellspacing="0" border="0"><tr>'
            /* Tags */
            + '<td style="width:500px;background:#FFF;vertical-align:top;border-left:3px solid #999;border-top:3px solid #999;border-bottom:3px solid #999;" rowspan="2">'
            + '<h3 style="font:bold 14px/14px sans-serif;margin:10px 0 5px 0;padding:5px;background:#CFC;">&#12459;&#12473;&#12479;&#12512;&#12522;&#12531;&#12463;</h3>'
            + '<div id="affiliate_custom_link">loading ...</div>'
            + '<textarea id="affiliate_custom_tag" cols="50" rows="5" style="width:500px;font:12px/12px monospace;"></textarea><br />'
            + '<h3 style="font:bold 14px/14px sans-serif;margin:20px 0 5px 0;padding:5px;background:#CFC;">&#12486;&#12461;&#12473;&#12488;&#12522;&#12531;&#12463;</h3>'
            + '<div id="affiliate_text_link">loading ...</div>'
            + '<input type="text" id="affiliate_text_tag" size="40" value="" style="width:500px;" /><br />'
            + '<h3 style="font:bold 14px/14px sans-serif;margin:20px 0 5px 0;padding:5px;background:#CFC;">&#30011;&#20687;&#12522;&#12531;&#12463;</h3>'
            + '<div id="affiliate_image_link">loading ...</div>'
            + '<input type="text" id="affiliate_image_tag" size="40" value="" style="width:500px;" /><br />'
            + '</td>'
            /* Controller */
            + '<td style="width:400px;background:#FFF;border-left:1px dashed #CCC;border-top:3px solid #999;border-right:3px solid #999;border-bottom:3px solid #999;vertical-align:top;">'
            + '<h3>&#30011;&#20687;&#12398;&#31278;&#39006;&#12539;&#12469;&#12452;&#12474;&#36984;&#25246;</h3>'
            + affiliate_images_html
            + '</td>'
            + '</tr>'
            /* Settings */
            + '<tr>'
            + '<td style="width:400px;vertical-align:top;border-left:3px solid #999;">'
            + '<h3>&#35373;&#23450;</h3>'
            + '<table cellpadding="0" cellspacing="1" border="0" style="background:#999;">'
            + '<tr>'
            + '<td style="text-align:right;width:120px;background:#CCC;">&#12450;&#12477;&#12471;&#12456;&#12452;&#12488;ID:</td>'
            + '<td style="background:#DDD;"><input type="text" size="15" id="affiliate_id" value="' + esc(affiliate_id) + '" /></td>'
            + '</tr>'
            + '<tr>'
            + '<td style="text-align:right;width:120px;background:#CCC;">&#24120;&#12395;&#34920;&#31034;:</td>'
            + '<td style="background:#DDD;"><input type="checkbox" id="affiliate_enable" value="1"'+(affiliate_enable>0?' checked=checked':'')+' /><label for="affiliate_enable">ON</label></td>'
            + '</tr>'
            + '<tr><td colspan="2" style="background:#DDD;">'
            + '<p style="margin:0;padding:5px;">'
            + '&#12459;&#12473;&#12479;&#12512;&#12522;&#12531;&#12463;&#65306;&#12486;&#12531;&#12503;&#12524;&#12540;&#12488;:<br />'
            + '<textarea cols="50" rows="5" id="affiliate_template" style="width:380px;font:12px/12px monospace;">' + esc(affiliate_template) + '</textarea><br />'
            + '</p>'
            + '<p style="margin:0;padding:5px;">'
            + '&#12486;&#12531;&#12503;&#12524;&#12540;&#12488;&#12391;&#20351;&#29992;&#21487;&#33021;&#12394;&#12461;&#12540;&#12527;&#12540;&#12489;:<br />'
            + '<strong>${aid}</strong> : &#12450;&#12477;&#12471;&#12456;&#12452;&#12488;ID<br />'
            + '<strong>${asin}</strong> : ASIN&#12467;&#12540;&#12489;<br />'
            + '<strong>${title}</strong> : &#21830;&#21697;&#21517;<br />'
            + '<strong>${url}</strong> : &#21830;&#21697;URL (&#12450;&#12477;&#12471;&#12456;&#12452;&#12488;ID&#21547;&#12416;)<br />'
            + '<strong>${imgid}</strong> : &#30011;&#20687;ID<br />'
            + '<strong>${img}</strong> : &#30011;&#20687;URL (${imgid}&#21547;&#12416;)<br />'
            + '</p>'
            + '</td></tr>'
            + '<tr><td colspan="2" style="text-align:center;background:#DDD;">'
            + '<input type="button" id="settings_submit" value="SAVE &amp; RELOAD" style="font:bold 14px/14px Arial;" />'
            + '</td></tr>'
            + '<tr><td colspan="2" style="background:#DDD;">'
            + '* This settings save to your browser as COOKIE<br />'
            + '</td></tr>'
            + '</table>'
            + '</tr></table>'
            + '</div>';

        block.setAttribute('id','quick_affiliate');
        block.style.border     = '1px dashed #000';
        block.style.background = '#EEE';
        block.style.margin     = '0 0 20px 0';
        block.style.padding    = '15px';
        d.getElementById('handleBuy').insertBefore(block, d.getElementById('handleBuy').firstChild);

        setAffiliateFormat();

        // Default style and EventListener settings
        if ( affiliate_enable > 0 ) {
            d.getElementById('affiliate_block').style.display = 'block';
            d.getElementById('af_switch_off').style.display   = 'inline';
            d.getElementById('af_switch_on').style.display    = 'none';
        } else {
            d.getElementById('affiliate_block').style.display = 'none';
            d.getElementById('af_switch_off').style.display   = 'none';
            d.getElementById('af_switch_on').style.display    = 'inline';
        }
        
        var radios = d.getElementById('affiliate_img_sizes').getElementsByTagName('input');
        for ( var i=0; i<radios.length; i++ ) {
            _AddEventListener(radios[i],'click',setAffiliateFormat);
        }
        var labels = d.getElementById('affiliate_img_sizes').getElementsByTagName('label');
        for ( var i=0; i<labels.length; i++ ) {
            _AddEventListener(labels[i],'click',setAffiliateFormat);
        }

        _AddEventListener(d.getElementById('settings_submit'),'click',saveSettings);
        _AddEventListener(
            d.getElementById('af_switch_on'),
            'click',
            function() {
                d.getElementById('affiliate_block').style.display = 'block';
                d.getElementById('af_switch_off').style.display   = 'inline';
                d.getElementById('af_switch_on').style.display    = 'none';
                l.hash = 'quick_affiliate';
            }
        );
        _AddEventListener(
            d.getElementById('af_switch_off'),
            'click',
            function() {
                d.getElementById('affiliate_block').style.display = 'none';
                d.getElementById('af_switch_off').style.display   = 'none';
                d.getElementById('af_switch_on').style.display    = 'inline';
            }
        );
    }

    /**
     * Crossbrowser addEventListener
     */
    function _AddEventListener(e, type, fn) {
        if (e.addEventListener) {
            e.addEventListener(type, fn, false);
        }
        else {
            e.attachEvent('on' + type, function() {
                fn(window.event);
            });
        }
    }

    // Do it! (only in product page)
    var bodys = d.getElementsByTagName('body');
    if ( bodys[0].getAttribute('class') == 'dp' ) {
        init();
    }

})();