// Twitter Clip
// 
// @based http://platform.twitter.com/bookmarklets/share.js?v=1

// == Bokmarklet ==
// javascript:(function(){window.twttr=window.twttr||{};var D=550,A=450,C=screen.height,B=screen.width,H=Math.round((B/2)-(D/2)),G=0,F=document,E;if(C>A){G=Math.round((C/2)-(A/2))}window.twttr.shareWin=window.open('http://twitter.com/share','','left='+H+',top='+G+',width='+D+',height='+A+',personalbar=0,toolbar=0,scrollbars=1,resizable=1');E=F.createElement('script');E.src='http://labs.creazy.net/bookmarklet/twitter_clip.js';F.getElementsByTagName('head')[0].appendChild(E)}());

(function ()
{
    function B(L)
    {
        var M = [], K = 0, J = L.length;
        for (; K < J; K++) {
            M.push(L[K])
        }
        return M
    }
    function G(J)
    {
        return encodeURIComponent(J).replace(/\+/g, "%2B")
    }
    function C(L)
    {
        var K = [], J;
        for (J in L) {
            if (L[J] !== null && typeof L[J] !== "undefined") {
                K.push(G(J) + "=" + G(L[J]))
            }
        }
        return K.sort().join("&")
    }
    function H()
    {
        var K = document.getElementsByTagName("a"), Q = document.getElementsByTagName("link"), J = /\bme\b/, 
        M = /^https?\:\/\/(www\.)?twitter.com\/([a-zA-Z0-9_]+)$/, P = B(K).concat(B(Q)), O, S, L, N = 0, 
        R;
        for (; (R = P[N]); N++)
        {
            S = R.getAttribute("rel");
            L = R.getAttribute("href");
            if (S && L && S.match(J) && (O = L.match(M))) {
                return O[2];
            }
        }
    }
    function F(K)
    {
        var J;
        if (K.match(/^https?:\/\//)) {
            return K
        }
        else
        {
            J = location.host;
            if (location.port.length > 0) {
                J += ":" + location.port
            }
            return [location.protocol, "//", J, K].join("");
        }
    }
    function I()
    {
        var J = document.getElementsByTagName("link");
        for (var K = 0, L; (L = J[K]); K++) {
            if (L.getAttribute("rel") == "canonical") {
                return F(L.getAttribute("href"));
            }
        }
        return null
    }
    function T() {
        // clipping settings
        var strMax  = 100;     // ~110
        var hashTag = '#clip'; // if you don't need it, put blank
        
        // selected sentence
        var sel = '';
        if ( document.selection ) {
            sel = document.selection.createRange().text;
        } else if ( window.selection ) {
            sel = window.selection.createRange().text;
        } else if ( document.getSelection ) {
            sel = document.getSelection();
        } else if ( window.getSelection ) {
            sel = window.getSelection();
        }
        if ( typeof(sel) == 'undefined' ) {
            sel = '';   
        } else {
            sel = sel.replace(/(\r\n|\r|\n|\t)/ig,' ');
            sel = sel.replace(/\s{2,}/ig,' ');
        }
        if ( sel.length > (strMax-hashTag.length-3) ) {
            sel = sel.substring(0,strMax);
        }

        var str = (sel=='') ? document.title : '"'+sel+'"';
        
        if ( hashTag ) {
            str = hashTag + ' ' + str;
        }
        

        return decodeURIComponent(str)
    }
    var D = (function ()
    {
        var K = 
        {
            //text : decodeURIComponent(document.title), url : (I() || location.href), _ : ((new Date()).getTime())
            text : T(), url : (I() || location.href), _ : ((new Date()).getTime())
        };
        var J = H();
        if (J) {
            K.via = J
        }
        return C(K)
    }
    ());
    var E = window.twttr.shareWin, A = "http://twitter.com/share?" + D;
    if (E && E.open) {
        E.location.href = A;
        E.focus()
    }
    else {
        window.location.href = A;
    }
}
());
