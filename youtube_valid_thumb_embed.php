/**
 * Youtube Valid Embed Code with Thumbnails
 * 
 * == Bookmarklet ==
 * javascript:(function(){var d=document,s=d.createElement('script');s.charset='UTF-8';s.src='http://labs.creazy.net/bookmarklet/youtube_valid_thumb_embed.php?v=1';d.getElementsByTagName('head')[0].appendChild(s);})();
 * 
 * @param  thumb { 0:no thumb(text) / 1~3:show n piece of thumbnails. }
 * @param  force_thumb { 1:force to show thumbnail / 0:show object tag }
 * @author yager <yager at creazy.net>
 * @see    http://creazy.net/2010/02/youtube_perfect_embed.html
 */

(function(){

var w   = window;
var d   = w.document;
var tag = d.getElementById('embed_code').value;
var c   = w.yt.config_; // YouTube config
var thumb       = <?= empty($_GET['thumb']) ? 1 : $_GET['thumb'] ?>;
var force_thumb = <?= empty($_GET['force_thumb']) ? 0 : $_GET['force_thumb'] ?>;

var title = d.getElementById('watch-headline-title').textContent || d.getElementById('watch-headline-title').innerText;
title = title.replace(/(\r\n|\r|\n|\t|\s)/g,'');
/**
 * Fixed validate error codes
 */

// get data src
var src   = '';
if ( tag.match(/<param\sname=\"movie\"\svalue=\"(.+?)\"/) ) {
    src = RegExp.$1;

    // replace first "&" to "?"
    src = src.replace(/&/,'?');

    // replace "&" to "&amp;"
    src = src.replace(/&/g,'&amp;');

    // add object attribute
    tag = tag.replace(/<object\s/,'<object data="'+src+'" type="application/x-shockwave-flash" ');

    // replace param
    tag = tag.replace(/<param\sname=\"movie\"\svalue=\"(.+?)\"/,'<param name="movie" value="'+src+'"');
}

// replace param end tag
tag = tag.replace(/><\/param>/g,' />');

/**
 * Add Alternative thumbnails
 */

alt_embed = '<div class="yt_embed_block">'
if ( thumb == 1 ) { // show defalut
    alt_embed
        += '<a href="http://www.youtube.com/watch?v='+c.VIDEO_ID+'" target="_blank" class="yt_embed_thumb">'
        +  '<img src="http://i.ytimg.com/vi/'+c.VIDEO_ID+'/default.jpg" alt="'+title+'" />'
        +  '</a><br />';
}
else if ( thumb == 2 || thumb == 3 ) { // show multi thumbnails
    for ( i=1; i<=thumb; i++ ) {
        alt_embed
            += '<a href="http://www.youtube.com/watch?v='+c.VIDEO_ID+'" target="_blank" class="yt_embed_thumb">'
            +  '<img src="http://i.ytimg.com/vi/'+c.VIDEO_ID+'/'+i+'.jpg" alt="'+title+'" />'
            +  '</a>';
    }
    alt_embed += '<br />';
} else {
    // not show thumbnails
}
alt_embed += '<a href="http://www.youtube.com/watch?v='+c.VIDEO_ID+'" target="_blank" class="yt_embed_title">'+title+'</a>'
alt_embed += '</div>'

if ( force_thumb ) {
    tag = alt_embed;
} else {
    tag = tag.replace(/<embed\s.+?<\/embed>/,alt_embed);
}
w.prompt('Perfect Embed Tag',tag);

})();
