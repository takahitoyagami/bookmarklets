// Youtube Switchable Embed

/*
<param name="movie" value="http://www.youtube.com/v/zlfKdbWwruY&hl=ja&fs=1&color1=0x3a3a3a&color2=0x999999&border=1">

<object width="660" height="405"><param name="movie" value="http://www.youtube.com/v/zlfKdbWwruY&hl=ja&fs=1&color1=0x3a3a3a&color2=0x999999&border=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/zlfKdbWwruY&hl=ja&fs=1&color1=0x3a3a3a&color2=0x999999&border=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="660" height="405"></embed></object>
<embed src="()"
&ap=%2526fmt%3D22
*/

var d = document;
var w = window;
var tag   = d.getElementById('embed_code').value;
var id_base = swfArgs['video_id']+'_'+(new Date().getTime());

function createXHR() {
    if ( w.ActiveXObject ) {
        try {
            return new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                return new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e2) {
                return null;
            }
        }
    } else if ( w.XMLHttpRequest ) {
        return new XMLHttpRequest();
    } else {
        return null;
    }
}

checkURL(swfArgs['video_id']);

/**
 * Checking
 */
function checkURL(video_id) {
    var url
        = 'http://'+location.host+'/watch'
        + '?v='+video_id
        + '&fmt=22';
    var XHR = createXHR();
    XHR.open( 'GET', url, true );
    XHR.onreadystatechange = function() { 
        if (XHR.readyState==4) {
            if ( match = XHR.responseText.match(/var swfArgs = ({.*})/) ) {
                json = eval('('+RegExp.$1+')');
                
                // Get object url
                if ( matches = tag.match(/<embed\ssrc=\"(.*?)\"/) ) {
                    var embed_url = matches[1];
                } else {
                    alert('error: failed to analyze tag.');
                    exit;
                }

                if ( json['fmt_map'] == '22/2000000/9/0/115' ) {
                    pattern   = '&ap=%2526fmt%3D22';
                    embed_msg = 'Normal/MP4:HD tag';
                } else {
                    pattern   = '&ap=%2526fmt%3D18';
                    embed_msg =  'Normal/MP4:iPod tag';
                }
                tag = tag.replace(/<param\sname=\"movie\"\svalue=\"(.*?)\">/,'<param name="movie" value="$1'+pattern+'"');
                tag = tag.replace(/<embed\ssrc=\"(.*?)\"/,'<embed src="$1'+pattern+'"');

                prompt(embed_msg,tag);
            }
        }
    }
    XHR.send('');
}
