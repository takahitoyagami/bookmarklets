<?php
mb_internal_encoding("UTF-8");
mb_language("japanese");
?>
/**
 * show HTML source
 * @author takahito.yagami[at]gmail[dot]com
 */
 
/**
 * get HTML source by file_get_contents, insert to DIV
 */
function __showSource() {
	var html
		= "<p style='text-align:right;margin:5px 10px 0 10px;padding:0;'>"
		+ "<a href='javascript:document.body.removeChild(document.getElementById(&quot;__source_viewer&quot;));void(0);'>close</a>"
		+ "</p>"
		+ "<text"+"area rows='20' style='display:block;margin:5px 10px 10px 10px;border:1px solid #000000;width:95%;height:85%;background:#FFFFFF;color:#000000;font:12px monospace;'>"
		+ "<?php
$htmls = file($_GET['url']);
$html  = "";
foreach( $htmls as $val ) {
	$html .= htmlspecialchars(mb_convert_encoding(rtrim($val),"UTF-8","auto"),ENT_QUOTES) . "&#10;";
}
//$html  = implode("¥n",file($_GET['url']));
//$html  = mb_convert_encoding($html,"UTF-8","auto");
//$html = htmlspecialchars($html,ENT_QUOTES);

//$html = str_replace("¥n","&#10;",$html);

//$html = mb_ereg_replace(0x0D.0x0A,"&#10;",$html);
//$html = mb_ereg_replace(0x0D,"&#10;",$html);
//$html = mb_ereg_replace(0x0A,"&#10;",$html);
//$html = nl2br($html);
//$html = mb_ereg_replace("<br />.","&#10;",$html);
echo $html;
?>"
		+ "</text"+"area>";
	document.getElementById('__source_viewer').innerHTML = html;
}

// prepare HTML source viewer
document.body.innerHTML
	+= '<div id="__source_viewer" style="position:absolute;z-index:99999;top:5%;left:5%;width:90%;height:90%;background:#EEEEEE;">loading ...</di'+'v>';
window.scrollTo(0,0);

__showSource();
