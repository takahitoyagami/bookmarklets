/**
 * Twitter Lists Cloud
 * 
 * Show TagCloud in Twitter user page by analyzing Lists.
 */

// variants
var lists_text    = '';
var lists_ranking = [];
var lists_count   = 0;

/**
 * create XmlHttpRequest
 */
function createXHR() {
    if ( window.ActiveXObject ) {
        try {
            return new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                return new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e2) {
                return null;
            }
        }
    } else if ( window.XMLHttpRequest ) {
        return new XMLHttpRequest();
    } else {
        return null;
    }
}

/**
 * check URL
 */
function checkList(url) {
    if ( !url ) {
        //console.log('invalid URL');
    }

    var XHR = createXHR();
    XHR.open( 'GET', url, true );
    XHR.onreadystatechange = function() { 
        if (XHR.readyState==4) {
            text = XHR.responseText;
            //console.log(text);

            if ( text.match(/<table\sid=\"lists_table\"\sclass=\"users-lists\">([\s\S]+?)<\/table>/i) ) {
                lists = RegExp.$1;
            }
            //console.log(lists);

            if ( lines = lists.match(/<tr.*?>([\s\S]+?)<\/tr>/ig) ) {
                lists_count += lines.length
                //console.log(lists_count+'件');
                lists_cloud_div.innerHTML
                    = '<img src="http://twitter.com/images/search/expanding.gif" alt="loading" style="vertical-align:middle;" />'
                    + ' Listsを ' + lists_count + '件 解析済み';
                for ( var i=0; i<lines.length; i++ ) {
                    //console.log(lines[i]);

                    if ( lines[i].match(/<span>\@(.+?)\/<wbr\/><b>(.+?)<\/b>/i) ) {
                        list_name = RegExp.$2;
                        //console.log('user='+RegExp.$1);
                        //console.log(list_name);
                        // Count list
                        if ( lists_text.indexOf("'"+list_name+"'") < 0 ) {
                            if ( lists_text != '' ) lists_text += ',';
                            lists_text += "'"+list_name+"'";
                            lists_ranking[lists_ranking.length] = {"name":list_name, "count":1, "users":[RegExp.$1]};
                        } else {
                            for ( var j=0; j<lists_ranking.length; j++ ) {
                                if ( lists_ranking[j].name == list_name ) {
                                    lists_ranking[j].count++;
                                    lists_ranking[j].users[lists_ranking[j].users.length] = RegExp.$1;
                                    break;
                                }
                            }
                        }
                    }

                }
            }
            // 次のページ
            if ( text.match(/<a\shref=\"(\/[0-9a-z_]{1,15}\/lists\/memberships\?.+?)\"/i) ) {
                next_url = RegExp.$1;
                next_url = next_url.replace(/&amp;/g,'&');
                //console.log(next_url);
                checkList(next_url)
            }
            // 出力
            else {
                // Sort
                //lists_ranking.sort(function(a,b){return b.count - a.count;});
                //console.log(lists_ranking);

                lists_cloud_div.innerHTML = 'Lists解析完了！<br />';
                
                for ( var i=0; i<lists_ranking.length; i++ ) {
                    lists_rate = lists_ranking[i].count/lists_count*100;
                    
                    lists_cloud_span = document.createElement('a');
                    lists_cloud_span.setAttribute('title',lists_ranking[i].name + '('+lists_ranking[i].count+'/'+lists_count+') '+Math.round(lists_rate)+'%');
                    lists_cloud_span.setAttribute('href',"javascript:showSelectedLists('"+lists_ranking[i].name+"',['"+lists_ranking[i].users.join("','")+"']);");
                    lists_cloud_span.innerHTML = lists_ranking[i].name;
                    //console.log(lists_ranking[i].users);
                    
                    lists_cloud_span.style.display = 'inline-block';
                    lists_cloud_span.style.font  = (14+Math.ceil(lists_rate*2))+'px/1.2 Arial,sans-serif';
                    lists_cloud_span.style.margin = '0 10px 0 10px';
                    lists_cloud_span.style.textDecoration = 'underline';
                    if ( Math.ceil(lists_rate) > 20 ) {
                        lists_cloud_span.style.color = '#333';
                        lists_cloud_span.style.fontWeight = 'bold';
                    }
                    else if ( Math.ceil(lists_rate) > 10 ) {
                        lists_cloud_span.style.color = '#555';
                        lists_cloud_span.style.fontWeight = 'bold';
                    }
                    else if ( Math.ceil(lists_rate) > 5 ) {
                        lists_cloud_span.style.color = '#777';
                        lists_cloud_span.style.fontWeight = 'bold';
                    }
                    else {
                        lists_cloud_span.style.color = '#999';
                    }
                    
                    lists_cloud_div.appendChild(lists_cloud_span);
                }
            }
        }
    }
    XHR.send('');
}

function showSelectedLists(list,users) {
    txt = "以下のユーザが "+list+" リストに追加しています\n";
    for ( var i=0; i<users.length; i++ ) {
        txt += "@"+users[i]+"\n";
    }
    alert(txt);
}

if ( location.pathname.match(/^\/([0-9a-z_]{1,15})/i) ) {
    var u = RegExp.$1;
    //console.log(u);

    var lists_cloud_div = document.createElement('div');
    lists_cloud_div.setAttribute('id','lists_cloud_div');
    lists_cloud_div.style.margin    = '10px';
    lists_cloud_div.style.font      = '20px/1.5 Arial';
    lists_cloud_div.style.textAlign = 'center';
    document.getElementById('content').insertBefore(lists_cloud_div,document.getElementById('content').firstChild);

    var lists_cloud_div = document.getElementById('lists_cloud_div');

    checkList('http://twitter.com/'+u+'/lists/memberships');
}
