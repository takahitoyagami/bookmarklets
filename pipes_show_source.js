/**
 * show HTML source (powered by Yahoo!Pipes)
 * @author takahito.yagami[at]gmail[dot]com
 */
 
/**
 * callback function
 * get HTML source from pipes, insert to DIV
 */
function __showSource(json) {
	var html
		= '<p style="text-align:right;margin:5px 10px 0 10px;padding:0;">'
		+ '<a href="javascript:document.body.removeChild(document.getElementById(\'__source_viewer\'));void(0);">close</a>'
		+ '</p>'
		+ '<text'+'area style="margin:5px 10px 10px 10px;border:1px solid #000000;width:95%;height:85%;background:#FFFFFF;color:#000000;font:12px monospace;">'
		+ json.value.items[0].content.replace(/</g,'&#60;').replace(/>/g,'&#62;')
		+ '</text'+'area>';
	document.getElementById('__source_viewer').innerHTML = html;
}

// prepare HTML source viewer
document.body.innerHTML
	+= '<div id="__source_viewer" style="position:absolute;z-index:99999;top:5%;left:5%;width:90%;height:90%;background:#EEEEEE;">loading ...</di'+'v>';
window.scrollTo(0,0);

// call pipes
var s=document.createElement('script');
s.charset='UTF-8';
s.src='http://pipes.yahoo.com/yager/page_loader?_render=json&_callback=__showSource&url='+encodeURIComponent(location.href);
document.body.appendChild(s)