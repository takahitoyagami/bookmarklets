// == Bokmarklet ==
// javascript:(function(){window.open('','twclip');window.bitly_id='{your bit.ly id}';window.bitly_key='your bit.ly API key';var d=document,s=d.createElement('script');s.type='text/javascript';s.src='http://labs.creazy.net/bookmarklet/twclip.js';d.getElementsByTagName('head')[0].appendChild(s);})();


window.twclippCallback = function(json) {
    var d = document;
    var w = window;
    var l = location;
    var e = encodeURIComponent;
    var limit = 130;
    // selected sentence
    var sel = '';
    if ( d.selection ) {
        sel = d.selection.createRange().text;
    } else if ( w.selection ) {
        sel = w.selection.createRange().text;
    } else if ( d.getSelection ) {
        sel = d.getSelection();
    } else if ( w.getSelection ) {
        sel = w.getSelection();
    }
    if ( typeof(sel) == 'undefined' ) {
        sel = '';   
    }
    sel = sel.replace(/(\r\n|\r|\n|\t)/ig,' ');
    sel = sel.replace(/\s{2,}/ig,' ');
    var url = (json==null) ? location.href : json.results[location.href]['shortUrl'];
    var str = (sel=='') ? d.title : sel;
    str = '#clip ' + str;
    
    if ( str.length > (limit-url.length) ) {
        str = str.substring(0,limit-url.length) + '…';
    }
    
    var s = str + ' ' + url;
    var f = 'http://twitter.com/home/?status=' + e(s);
    if( !w.open(f,'twclip') ) {
        l.href = f;
    }
}

if ( typeof(bitly_id) != 'undefined' && typeof(bitly_key) != 'undefined' ) {
    api = 'http://api.bit.ly/shorten'
        + '?version=2.0.1'
        + '&format=json'
        + '&callback=window.twclippCallback'
        + '&login=' + bitly_id
        + '&apiKey=' + bitly_key
        + '&longUrl=';
        
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = api + encodeURIComponent(location.href);
    document.getElementsByTagName('head')[0].appendChild(script);
} else {
    window.twclippCallback();
}
