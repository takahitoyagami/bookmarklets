// @author         yager
// @email          yager@creazy.net
// @name           Skype HTML History Report
// @namespace      http://creazy.net/
// @description    Genrate Some Report Formats from Skype HTML History Page

// Bookmarklet
// javascript:(function(){var s=document.createElement('script');s.charset='UTF-8';s.src='http://labs.creazy.net/bookmarklet/skype_html_history_report.js';document.body.appendChild(s)})();

(function() {

	var d  = document;
	var dls = d.getElementsByTagName('dl');

	var list = [];
	var list_count = 0;
	var dy = '';
	var dt = '';
	var dd = '';
	var nm = '';
	var tm = '';
	for ( var i=0; i<dls.length; i++ ) {
		dls[i].innerHTML = dls[i].innerHTML.replace(/(<dd><\/dd>)/g,'');
		childs = dls[i].childNodes;
		for ( var j=0; j<childs.length; j++ ) {
			switch ( childs[j].nodeName ) {
				case 'H3':
					dy = childs[j].textContent;
					if ( !dy ) {
						alert('replace h3');
						dy = childs[j].innerText;
					}
					break;
				case 'DT':
					dt = childs[j].textContent;
					if ( !dt ) {
						dt = childs[j].innerText;
					}
					nm = dt.replace(/^(.*?):\s\d{2}:\d{2}:\d{2}\s?$/,'$1');
					tm = dt.replace(/^.*?:\s(\d{2}:\d{2}:\d{2})\s?$/,'$1');
					break;
				case 'DD':
					dd = childs[j].innerHTML;
					if ( dd ) {
						if ( dd.match(/<H3>(.*)<\/H3>/i) ) {
							dy = dd.replace(/<H3>(.*)<\/H3>/i,'$1');
							break;
						} else {
							list[list_count++] = [dy+' '+tm,nm,dd];
						}
					}
					dt = '';
					dd = '';
					nm = '';
					tm = '';
					break;
				default:
					// nothig to do
			}
		}
	}

	// csv format
	var txt = '';
	for ( var i=0; i<list.length; i++ ) {
		txt += esccsv(list[i][0]) + ","
             +  esccsv(list[i][1]) + ","
             +  esccsv(list[i][2])  + "\n";

	}
	var hrElement = d.createElement('hr');
	d.body.appendChild(hrElement);

	var textElement = d.createElement('h4');
	textElement.innerHTML = 'CSV形式';
	d.body.appendChild(textElement);

	var csvElement = d.createElement('textarea');
	csvElement.value = txt;
	csvElement.style.width    = "800px";
	csvElement.style.height   = "100px";
	csvElement.style.fontSize = "11px";
	d.body.appendChild(csvElement);

	// text format
	txt = '';
	for ( var i=0; i<list.length; i++ ) {
		txt += "["+list[i][0].replace(/.*(\d{2}:\d{2}:\d{2})$/,'$1')+"] "
             + list[i][1] + ": " + list[i][2]  + "\n";

	}
	hrElement = d.createElement('hr');
	d.body.appendChild(hrElement);

	textElement = d.createElement('h4');
	textElement.innerHTML = 'TEXT形式';
	d.body.appendChild(textElement);

	var csvElement = d.createElement('textarea');
	csvElement.value = txt;
	csvElement.style.width    = "800px";
	csvElement.style.height   = "100px";
	csvElement.style.fontSize = "11px";
	d.body.appendChild(csvElement);

	// wiki format
	txt = '';
	for ( var i=0; i<list.length; i++ ) {
		txt += "* "+list[i][0].replace(/.*(\d{2}:\d{2}:\d{2})$/,'$1')
             + " (" + list[i][1] + ")\n"
             + "** " + list[i][2]  + "\n";
	}

	hrElement = d.createElement('hr');
	d.body.appendChild(hrElement);

	textElement = d.createElement('h4');
	textElement.innerHTML = 'Wiki形式';
	d.body.appendChild(textElement);

	var wikiElement = d.createElement('textarea');
	wikiElement.value = txt;
	wikiElement.style.width    = "800px";
	wikiElement.style.height   = "100px";
	wikiElement.style.fontSize = "11px";
	d.body.appendChild(wikiElement);

	location.hash = 'end_of_document';

	function esccsv(str) {
		return '"'+str.replace(/"/g,'""')+'"';
	}
	function eschtml(str) {
		str = str.replace(/&/g,'&amp;');
		str = str.replace(/</g,'&lt;');
		str = str.replace(/>/g,'&gt;');
		return str;
	}

})();
