/**
 * DoYouKnow.js
 * 
 * If you execute this script as a Bookmarklet,
 * the selected text will be converted into the form transrated by iKnow API.
 * 
 * = Bookmarklet =
 * javascript:var w=window,d=document,s=d.createElement('script');s.charset='UTF-8';s.src='http://labs.creazy.net/bookmarklet/doyouknow.js';d.body.appendChild(s);void(0);
 * 
 * @author     yager <yager[at]creazy.net>
 * @namespace  http://creazy.net/
 * @version    1.0.0
 * @license    new BSD license
 * @copyright  (c) yager
 * 
 * @see http://creazy.net/2008/10/yahoo_pipes_translate_en_ja_api.html
 * 
 */

/**
 * part of speech (en -> ja)
 * @see http://ja.wikipedia.org/wiki/%E5%93%81%E8%A9%9E
 * @see http://www.alse-net.com/column/grammar-terms.htm
 */
var PART_OF_SPEECH = {
	'Adjective'         : '形容詞',
	'Adverb'            : '副詞',
	'Auxillary verb'    : '助動詞',
	'Conjunction'       : '接続詞',
	'Interrogative'     : '疑問詞',
	'Noun'              : '名詞',
	'Phrasal Verb'      : '句動詞',
	'Phrase'            : '句',
	'Preposition'       : '前置詞',
	'Pronoun'           : '代名詞',
	'Verb'              : '動詞',
	'Interjection'      : '感動詞',
	'Noun Abbreviation' : '略語',
	'Proper Noun'       : '固有名詞',
	'Particle'          : '助詞',
	'Verbal Noun'       : '動名詞',
	'Adjectival Noun'   : '形容名詞',
	'Kana'              : 'かな'
};

function __dykGetSelectedText() {
	var sel = '';
	if ( d.selection ) {
		sel = d.selection.createRange().text;
	} else if ( w.selection ) {
		sel = w.selection.createRange().text;
	} else if ( d.getSelection ) {
		sel = d.getSelection();
	} else if ( w.getSelection ) {
		sel = w.getSelection();
	}
	return sel;
}

function __dykGetScrollTop() {
	var top = d.documentElement.scrollTop || d.body.scrollTop;
	return top;
}

function __dykGetScrollHeight() {
	var h = d.body.scrollHeight || d.documentElement.scrollHeight;
	return h;
}

function __dykEscapeHtml(str) {
	str = str.replace(/&/g, "&amp;");
	str = str.replace(/"/g, "&quot;");
	str = str.replace(/'/g, "&#039;");
	str = str.replace(/</g, "&lt;");
	str = str.replace(/>/g, "&gt;");
	return str;
}
function __dykUnescapeHtml(str) {
	str = str.replace(/&quot;/g, '"');
	str = str.replace(/&#039;/g, "'");
	str = str.replace(/&lt;/g,   "<");
	str = str.replace(/&gt;/g,   ">");
	str = str.replace(/&amp;/g,  "&");
	return str;
}

function __dykTranslateWord(json) {
	var ele = d.getElementById('__dyk_results');

	// word mode
	html = '';
	if ( json.length == 0 ) {
		html += '<p>no data</p>';
	} else {
		html += '<ol style="height:300px;overflow:auto;margin:0 0 0 5px;padding:0 0 0 30px;list-style-type:decimal">';
		for ( var i=0; i<json.length; i++ ) {
			res = json[i].responses[0];
			cue = json[i].cue;
			html += '<li style="margin:0;padding:10px 0 0 0;font:12px/16px sans-serif;border-bottom:1px dashed #CCC;">';
			html += '<strong style="font:bold 12px/16px sans-serif;">'+__dykMakeResearchLink(cue.text)+'</strong> ';
			html += '['+PART_OF_SPEECH[cue.part_of_speech]+'] ';
			if ( cue.sound ) {
				html += '<a href="'+cue.sound+'" target="iknow_sound"><img src="http:/'+'/labs.creazy.net/bookmarklet/images/sound.gif" alt="play" border="0" style="vertical-align:middle;" /></a> ';
			}
			html += '<ul style="margin:0;padding:0 0 0 20px;">';
			html += '<li style="font:12px/16px sans-serif;">'+__dykMakeResearchLink(res.text)+'</li>';

			sens = json[i].sentences;
			if ( sens.length > 0 ) {
				for ( var j=0; j<sens.length; j++ ) {
					html += '<li style="font:12px/16px sans-serif;">'+__dykMakeResearchLink(sens[j].text)+'<br />';
					html += '('+__dykMakeResearchLink(sens[j].translations.text)+')</li>';
				}
			}
			html += '</ul>';
			html += '</li>';
		}
		html += '</ol>';
	}
	
	ele.innerHTML += html;
}
function __dykTranslateSentence(json) {
	var ele = d.getElementById('__dyk_results');

	// sentence mode
	html = '=== Sentence ===<br />';
	if ( json.length == 0 ) {
		html += 'no data<br />';
	} else {
		for ( var i=0; i<json.length; i++ ) {
			cue = json[i].text;
			res1 = json[i].transliterations.kana;
			res2 = json[i].transliterations.romaji;
			html += cue +'<br />';
			html += res1 +'/<a href="'+ json[i].sound +'" target="iknow_sound">play</a><br />';
			html += res2 +'/<a href="'+ json[i].sound +'" target="iknow_sound">play</a><br />';
		}
	}
	
	ele.innerHTML += html;
}

function __dykOpenBox() {
	if ( d.getElementById('__dyk_box') ) {
		return;
	} else {
		var sel = __dykGetSelectedText();
		
		scBox = d.createElement("div");
		scBox.setAttribute('id','__dyk_box');
		with(scBox.style){
			textAlign="left";
			margin="0";
			padding="0";
			position="absolute";
			top="0";
			left="0";
			zIndex=9990;
			width="100%";
			height=__dykGetScrollHeight()+"px";
			border=0;
			overflow="hidden";
		};

		scBg = d.createElement("div");
		with(scBg.style){
			position="fixed";
			top="0";
			left="0";
			width="100%";
			height="100%";
			background="#000000";
			filter="alpha(opacity=50)";
			mozOpacity=0.5;
			opacity=0.5;
		};
		__dykAddEventListener(scBg,'click',__dykCloseBox);
		scBox.appendChild(scBg);

		scMain = d.createElement("div");
		scMain.setAttribute('id','__dyk_main');
		with(scMain.style){
			position="absolute";
			top = __dykGetScrollTop()+"px";
			left="0";
			zIndex=9991;
			marginTop="10px";
			marginLeft="10px";
			padding="0";
			width="400px";
			background="#EEEEEE";
			color="#000000";
			border="1px solid #000";
		};
		html
			= '<form id="__dyk_form" onsubmit="__dykExec();return false;" style="margin:0;padding:0;">'
			+ '<p style="margin:0;padding:0;height:26px;background:#333;color:#FFF;">'
			+   '<a href="javascript:__dykCloseBox();" style="font:26px/26px sans-serif;padding:0 5px;background:#000;color:#FFF;text-decoration:none;float:right;">&#215;</a>'
			+   '<span style="font:26px/26px sans-serif;padding:0 5px;">SelectConvert</span>'
			+ '</p>'
			+ '<p style="clear:both;text-align:left;margin:0;padding:5px;border-bottom:1px solid #999;">'
			+   '<textarea name="__dyk_q" id="__dyk_q" style="width:300px;height:40px;font:12px/15px monospace;vertical-align:middle;" tabindex="100">'+sel+'</textarea>'
			+   ' <input type="submit" value="&#22793;&#25563;" style="font:14px/20px sans-serif;vertical-align:middle;" />'
			+ '</p>'
			+ '<div id="__dyk_results" style="margin:0;padding:5px;"></div>'
			+ '<iframe width="1" height="1" name="iknow_sound" style="visibility:hidden;"></iframe>'
			+ '</form>';
		scMain.innerHTML = html;
		scBox.appendChild(scMain);

		d.body.appendChild(scBox);

		__dykAddEventListener(d,'keydown',function(e){_dykEsc(e);});

	}
}

function __dykResearch(str) {
	d.getElementById('__dyk_q').value = __dykUnescapeHtml(str);
	__dykExec();
}
function __dykMakeResearchLink(str) {
	return '<a href="javascript:__dykResearch(&#039;'+__dykEscapeHtml(__dykEscapeHtml(str))+'&#039;);">'+__dykEscapeHtml(str)+'</a>';
}

var _dyk_form_index = 0;
function __dykExec() {
	var sel = d.getElementById('__dyk_q').value;
	var ele = d.getElementById('__dyk_results');
	if ( sel == '' ) {
		ele.innerHTML = '&#8593;&#22793;&#25563;&#12377;&#12427;&#25991;&#23383;&#12434;&#20837;&#21147;&#12375;&#12390;&#19979;&#12373;&#12356;&#12290;';
		d.getElementById('__dyk_q').focus();
		return;
	} else {
		ele.innerHTML = '';
	}
	_dyk_form_index = 0

	is_zen = 0;
	for (var i=0; i<sel.length; i++) {
		if ( escape(sel.charAt(i)).length >= 4 ) {
			//alert(sel.charAt(i) +"/"+escape(sel.charAt(i)));
			is_zen = 1;
			break;
		}
	}
	if ( is_zen ) {
		word_api
			= 'http://api.iknow.co.jp/items/matching/'+encodeURIComponent(sel)+'.json'
			+ '?callback=__dykTranslateWord'
			+ '&include_sentences=true'
			+ '&per_page=20'
			+ '&language=ja'
			+ '&translation_language=en';
		sentence_api
			= 'http://api.iknow.co.jp/sentences/matching/'+encodeURIComponent(sel)+'.json'
			+ '?callback=__dykTranslateSentence&require_sound=true'
			+ '&language=en';
		debug_api
			= 'http://api.iknow.co.jp/items/matching/'+encodeURIComponent(sel)+'.xml'
			+ '?include_sentences=true'
			+ '&per_page=20'
			+ '&language=ja'
			+ '&translation_language=en';
		ele.innerHTML = 'Japanese -&gt; English <a href="'+debug_api +'" target="_blank">debug</a>';
	} else {
		word_api
			= 'http://api.iknow.co.jp/items/matching/'+encodeURIComponent(sel)+'.json'
			+ '?callback=__dykTranslateWord'
			+ '&include_sentences=true'
			+ '&language=en'
			+ '&translation_language=ja';
		sentence_api
			= 'http://api.iknow.co.jp/sentences/matching/'+encodeURIComponent(sel)+'.json'
			+ '?callback=__dykTranslateSentence&require_sound=true'
			+ '&language=ja';
		debug_api
			= 'http://api.iknow.co.jp/items/matching/'+encodeURIComponent(sel)+'.xml'
			+ '?include_sentences=true'
			+ '&per_page=20'
			+ '&language=en'
			+ '&translation_language=ja';
		ele.innerHTML = 'English -&gt; Japanese <a href="'+debug_api +'" target="_blank">debug</a>';
	}

	// translate
	s = document.createElement('script');
	s.setAttribute('type','text/javascript');
	s.setAttribute('charset','utf-8');
	s.setAttribute('src',word_api);
	d.body.appendChild(s);
/*
	s = document.createElement('script');
	s.setAttribute('type','text/javascript');
	s.setAttribute('charset','utf-8');
	s.setAttribute('src',sentence_api);
	d.body.appendChild(s);
*/
	d.getElementById('__dyk_q').focus();
}

function __dykWrite(type,name,value) {
	var ele = d.getElementById('__dyk_results');

	var res = '';
	if ( type == 'text' ) {
		res = '<input type="text" style="font:11px/15px monospace;width:270px;padding:0;border:1px solid #CCC;" value="'+__dykEscapeHtml(value)+'" tab onfocus="this.select();" onmouseover="this.select();" id="_dyk_form_'+_dyk_form_index+'" tabindex="'+(100+_dyk_form_index)+'" />';
	} else if ( type == 'textarea' ) {
		res = '<textarea style="font:11px/15px monospace;width:270px;height:40px;border:1px solid #CCC;" onfocus="this.select();" onmouseover="this.select();" id="_dyk_form_'+_dyk_form_index+'" tabindex="'+(100+_dyk_form_index)+'">'+__dykEscapeHtml(value)+'</textarea>';
	} else {
		res = __dykEscapeHtml(value);
	}

	var html
		= '<table style="border-collapse:separate;border-spacing:5px;border:0;"><tr>'
		+ '<td style="text-align:right;width:100px;padding:0;font:11px/15px sans-serif;">'+name+':</td>'
		+ '<td style="text-align:left;width:275px;padding:0;font:11px/15px sans-serif;">'+res+'</td>'
		+ '</tr></table>';
	ele.innerHTML += html;

	_dyk_form_index++;
}

function __dykCloseBox() {
	if ( d.getElementById('__dyk_box') ) {
		d.body.removeChild(d.getElementById('__dyk_box'));
	}
}

function _dykEsc(e) {
    if (w.event != undefined) {
		keycode = w.event.keyCode;
    } else { 
        keycode = e.which;
    }
    if (keycode == 27) __dykCloseBox();
}

function __dykAddEventListener(e, type, fn) {
	if (e.addEventListener) {
		e.addEventListener(type, fn, false);
	}
	else {
		e.attachEvent('on' + type, function() {
			fn(window.event);
		});
	}
}

/**
 * Execution
 */
__dykOpenBox();
__dykExec();
