/**
 * Tumblr. Multi User Login Bookmarklet
 *
 * Bookmark url below
 * javascript:(function(){tid='YOUR_TUMBLR_ACCOUNT';tpw='YOUR_TUMBLR_PASSWORD';var d=document,w=window,e=w.getSelection,k=d.getSelection,x=d.selection,s=(e?e():(k)?k():(x?x.createRange().text:0));tbq=encodeURIComponent(s);var js=document.createElement('script');js.src='http://labs.creazy.net/bookmarklet/tumblr.js';document.body.appendChild(js);})();
 * Don't forget to rewrite tid | tpw in yours.
 *
 * @author yager <takahito.yagami[ at ]gmail.com>
 * @url http://creazy.net/
 * 
 * @version 1.0 2007/11/13
 */

// necessary to re-encode with non-Firefox browser
if(/Safari/.test(navigator.userAgent) || /MSIE/.test(navigator.userAgent) || /Opera/.test(navigator.userAgent)) {
	tbq=encodeURIComponent(tbq);
}

// create html
var tx
= '<span style="font:12px/20px Georgea,serif;">Login as '+tid+'</sp'+'an><br />'
+ '<iframe id="tumblr_manager_window" name="tumblr_manager_window" src="http://www.tumblr.com/logout" style="margin:0 auto 0 auto;width:95%;height:200px;border:0;"></i'+'frame>'
+ '<form name="tumblr_manager_form" action="http://www.tumblr.com/login" method="post" target="tumblr_manager_window" style="margin:0;padding:0;">'
+ '<input type="hidden" name="email" value="'+tid+'" />'
+ '<input type="hidden" name="password" value="'+tpw+'" />'
+ '</fo'+'rm>'
+ '<div style="font:26px/30px Georgia,serif;text-align:center;">'
+ '<a href="http://www.tumblr.com/dashboard" style="color:#000066;text-decoration:none;">Dashboard</'+'a>'
+ ' ||||| '
+ '<a href="javascript:var d=document,w=window,l=d.location,e=encodeURIComponent,u=\'http://www.tumblr.com/share?v=3&u=\'+e(l.href) +\'&t=\'+e(d.title) +\'&s=\'+\''+tbq+'\';try{if(!/^(.*\.)?tumblr[^.]*$/.test(l.host))throw(0);tstbklt();}catch(z){a =function(){if(!w.open(u,\'t\',\'toolbar=0,resizable=0,status=1,width=450,height=430\'))l.href=u;};if(/Firefox/.test(navigator.userAgent))setTimeout(a,0);else a();}void(0);" style="color:#000066;text-decoration:none;">Reblog</'+'a>'
+ ' |||||||||| '
+ '<a href="javascript:document.body.removeChild(document.getElementById(\'tumblr_manager\'));void(0);" style="font:10px/30px Georgia,serif;vertical-align:middle;color:#000066;text-decoration:none;">[X] close</'+'a>'
+ ' | '
+ '<span style="font:10px/30px Georgia,serif;vertical-align:middle;">powerd by <a href="http://creazy.net/" target="_blank" style="color:#000066;text-decoration:none;">creazy photograph</'+'a></sp'+'an>'
+ '</d'+'iv>';

// create re-login form
if ( !document.getElementById('tumblr_manager') ) {
	var dv
	= '<div id="tumblr_manager" style="position:absolute;text-align:center;top:0px;left:0px;z-index:99;width:100%;height:260px;background:#DDDDEE;color:#000000;">'
	+ '</d'+'iv>';
	document.body.innerHTML = document.body.innerHTML + dv;
}

// output
document.getElementById('tumblr_manager').innerHTML = tx;

// re-login submit
document.tumblr_manager_form.submit();
window.scrollTo(0,0);
