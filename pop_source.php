<?php
/**
 * HTMLソース表示ブックマークレット
 * 
 * [使い方]
 * ソースを確認したいページで下記ブックマークレットを実行
 * javascript:(function(){window.open('http://labs.creazy.net/bookmarklet/pop_source.php?url='+encodeURIComponent(location.href),'_blank');})();
 * 
 * [備考]
 * ソースをハイライトするために、GeSHiライブラリ（http://qbnz.com/highlighter/）を使用しています。
 * 
 * @author Takahito Yagami takahito.yagami[at]gmail[dot]com
 * @param url string ソースを表示したいページのURL
 * @see GeSHi - Generic Syntax Highlighter :: Home http://qbnz.com/highlighter/
 */
mb_internal_encoding("UTF-8");
mb_language("japanese");

// get HTML contents
if ( !$html = mb_convert_encoding( file_get_contents($_GET['url']),"UTF-8","auto") ) {
	die("Failed to get contents.");
}

// get title
if ( preg_match ( '/<title>(.*)<\/title>/i', $html, $match ) ){
	$title = $match[1];
} else {
	$title = "";
}

// replace tab to 4 spaces
$html = str_replace("	","    ",$html);

//----------------------------------------------------------
// Syntax Highlight by GeSHi(Generic Syntax Highlighter)
//----------------------------------------------------------
require_once("/virtual/creazy/public_html/lib/geshi/geshi.php");
$geshi = new GeSHi($html, "html4strict");                              // HTML mode initialize
$geshi->enable_line_numbers(GESHI_FANCY_LINE_NUMBERS, 2);              // show line numbers
$geshi->set_line_style('background: #FFFFFF;','background: #EEEEEE;'); // line colors

?>
<html>
<head>
<title>ソース：<?= $title ?></title>
<style type="text/css">
body {
	background: #FFFFFF;
}
ol {
	border: 3px solid #999999;
	width: 100%;
	background: #CCCCCC;
}
</style>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript"></script>
<script type="text/javascript">
_uacct = "UA-748843-3";
urchinTracker();
</script>
</head>

<body>

<p>orijinal page : <a href="<?= $_GET['url'] ?>"><?= $_GET['url'] ?></a></p>

<?= $geshi->parse_code() ?>

</body>
</html>
