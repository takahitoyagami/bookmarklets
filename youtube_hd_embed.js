// Youtube HD Embed

var d   = document;
var w   = window;
var tag = d.getElementById('embed_code').value;
var s   = w.yt.config_.SWF_ARGS;

function createXHR() {
    if ( w.ActiveXObject ) {
        try {
            return new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                return new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e2) {
                return null;
            }
        }
    } else if ( w.XMLHttpRequest ) {
        return new XMLHttpRequest();
    } else {
        return null;
    }
}

checkURL(s['video_id']);

/**
 * Checking
 */
function checkURL(video_id) {
    var url
        = 'http://'+location.host+'/watch'
        + '?v='+video_id
        + '&fmt=22';
    var XHR = createXHR();
    XHR.open( 'GET', url, true );
    XHR.onreadystatechange = function() { 
        if (XHR.readyState==4) {
            if ( match = XHR.responseText.match(/'SWF_ARGS': ({.*})/) ) {
                json = eval('('+RegExp.$1+')');
                var map = decodeURIComponent(json['fmt_url_map']);
                
                if ( map.indexOf('22|') > -1 ) {
                    pattern   = '&ap=%2526fmt%3D22';
                    embed_msg = 'Copy & Past the MP4(High Definition) tag';
                }
                else if ( map.indexOf('35|') > -1 ) {
                    pattern   = '&ap=%2526fmt%3D35';
                    embed_msg = 'Copy & Past the FLV(High Quality) tag';
                } else {
                    pattern   = '&ap=%2526fmt%3D18';
                    embed_msg = 'Copy & Past the MP4(for iPod) tag';
                }
                tag = tag.replace(/<param\sname=\"movie\"\svalue=\"(.*?)\">/,'<param name="movie" value="$1'+pattern+'"');
                tag = tag.replace(/<embed\ssrc=\"(.*?)\"/,'<embed src="$1'+pattern+'"');

                prompt(embed_msg,tag);
            }
        }
    }
    XHR.send('');
}
