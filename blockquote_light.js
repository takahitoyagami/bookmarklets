/**
 *
 */
(function(){
	var d = document;
	var w = window;
	var u = location.href;
	var t = d.title;
	if ( d.selection ) {
		src = d.selection.createRange().text;
	} else if ( w.selection ) {
		src = w.selection.createRange().text;
	} else if ( d.getSelection ) {
		src = d.getSelection();
	} else if ( w.getSelection ) {
		src = w.getSelection();
	} else {
		src = '';
	}
	src = src.replace(/\n/g,"<br />");
	var q
		= '<blockquote cite="'+u+'" title="'+t+'">'
		+ '<p>'+src+'</p>'
		+ '<cite>via: <a href="'+u+'" target="_blank">' +t+'</a></cite>'
		+ '</blockquote>';
	var r = w.prompt('Copy Blockquote',q);
})();
