/**
 * CopyLink.JS
 * 
 * # execute bookmarklet below
 * javascript:(function(){var s=document.createElement('script');s.charset='UTF-8';s.src='http://labs.creazy.net/bookmarklet/copylink.js';document.body.appendChild(s)})();
 * 
 * @author  Takahito Yagami <takahito.yagami[at]gmail[dot]com> (a.k.a yager)
 * @version v1.0.0 2008/05/07
 */
d = document;
w = window;

/**
 * VARIANT
 * _URL_    : page url
 * _TITLE_  : page title
 * _SELECT_ : a part you selected
 */
if ( d.selection ) {
	_SELECT_ = d.selection.createRange().text;
} else if ( w.selection ) {
	_SELECT_ = w.selection.createRange().text;
} else if ( d.getSelection ) {
	_SELECT_ = d.getSelection();
} else if ( w.getSelection ) {
	_SELECT_ = w.getSelection();
} else {
	_SELECT_ = '';
}
_URL_    = __escapeAmp(location.href);
_TITLE_  = __escapeAmp(d.title);
_SELECT_ = __escapeAmp(_SELECT_.replace(/\n/g,"<br />"));

//------------------------------------------------------------
// Functions
//------------------------------------------------------------

/**
 * Add a new link template
 */
var formats = new Array();
function __addLinkTemplate( name, template ) {
	formats[formats.length] = { "name":name, "template":template};
}

/**
 * HTML escape
 */
function __escapeHTML( str ) {
	str = str.replace(/"/g,"&quot;"); // "
	str = str.replace(/</g,"&lt;");   // <
	str = str.replace(/>/g,"&gt;");   // >
	return str;
}

/**
 * & escape
 */
function __escapeAmp( str ) {
	str = str.replace(/&/g,"&amp;");  // &
	return str;
}

/**
 * close Link Block
 */
function __closeLinkBlock() {
	document.body.removeChild(document.getElementById('__copy_link_block__'));
}

//------------------------------------------------------------
// Templates
//------------------------------------------------------------
__addLinkTemplate(
	'Copy Title Link',
	'<a href="'+_URL_+'" target="_blank">'+_TITLE_+'</a>'
);
__addLinkTemplate(
	'Copy URL Link',
	'<a href="'+_URL_+'" target="_blank">'+_URL_+'</a>'
);
__addLinkTemplate(
	'Copy Bloclquote',
	'<cite>via: <a href="'+_URL_+'" target="_blank">' +_TITLE_+'</a></cite>'
	+'<blockquote cite="'+_URL_+'" title="'+_TITLE_+'">'
	+'<p>'+_SELECT_+'</p>'
	+'</blockquote>'
);
__addLinkTemplate(
	'Copy Title + Hatebu Link',
	'<a href="'+_URL_+'" target="_blank">'+_TITLE_+'</a>'
	+'<a href="http://b.hatena.ne.jp/entry/'+_URL_+'" target="_blank">'
	+'<img src="http://b.hatena.ne.jp/entry/image/small/'+_URL_+'" alt="" border="0" />'
	+'</a>'
);
__addLinkTemplate(
	'Copy Screenshot(HeartRails Capture) Link',
	'<a href="'+_URL_+'" target="_blank"><img title="'+_TITLE_+'" src="http://capture.heartrails.com/small?'+_URL_+'" alt="'+_URL_+'" width="120" height="90" border="0" /></a>'
);

//------------------------------------------------------------
// Output
//------------------------------------------------------------
out = '<p><input type="button" value="X" onclick="__closeLinkBlock();" /></p>';
out += '<dl>';
for ( i=0; i<formats.length; i++ ) {
	out += '<dt>'+formats[i].name+'<dt>';
	out += '<dd>'+formats[i].template+'<dd>';
	out += '<dd>';
	out += '<input type="text" id="__copy_link_'+i+'" value="'+__escapeHTML(formats[i].template)+'" onfocus="this.select();" tabindex="'+(100+i)+'" />';
	out += '<dd>';
}
out += '</dl>';
d.body.innerHTML += '<div id="__copy_link_block__" style="position:absolute;top:0;left:0;background:#FFFFFF;text-align:left;">'+out+'</div>';
d.getElementById('__copy_link_0').focus();