// ==UserScript==
// @name           TweetTheMinutes
// @namespace      http://creazy.net/
// @include        http://twitter.com/*
// @include        https://twitter.com/*
// ==/UserScript==

(function() {

    var w = (typeof(unsafeWindow) != 'undefined') ? unsafeWindow : window;

    /**
     * Add cookie
     */
    w.addCookie = function(key, value) {
        if ( !key ) {
            return false;
        }
        document.cookie
            = key + '=' + escape(value) + '; '
            + 'expires=Tue, 1-Jan-2030 00:00:00 GMT; '
            + 'path=/; ';
    }
    /**
     * Get cookie
     */
    w.getCookie = function(key) {
        var cookies = document.cookie.split(';');
        for ( var i=0; i<cookies.length; i++ ) {
            if ( cookies[i].indexOf('=') < 0 ) continue;
            key_value  = cookies[i].replace(/(^[\s]+|;)/g,'');
            deli_index = key_value.indexOf('=');
            if ( key_value.substring(0,deli_index) == key ) {
                return unescape(key_value.substring(deli_index+1,key_value.length));
            }
        }
        return '';
    }

    /**
     * Save Settings to cookie
     */
    w.saveSettings = function() {
        w.addCookie('ttm_template', document.getElementById('ttm_template').value);
        w.addCookie('ttm_speakers', document.getElementById('ttm_speakers').value);
        alert('Saved');
        w.hideSettings();
    }

    w.applyTemplate = function(str) {
        document.getElementById('status').value
            = document.getElementById('ttm_template').value
            .replace(/(\{\$speaker\})/g,str)
            .replace(/(\{\$status\})/g,document.getElementById('status').value);
        document.getElementById('status').focus();
        document.getElementById('status').setSelectionRange(
            document.getElementById('status').value.length,
            document.getElementById('status').value.length
        );
        return true;
    }

    w.showSettings = function() {
        document.getElementById('ttm_menu').style.display = '';
        document.getElementById('ttm_setting').href      = 'javascript:hideSettings();';
    }
    w.hideSettings = function() {
        var speakers = w.esc(w.getCookie('ttm_speakers')).split(' ');
        var speaker_html = '<input type="button" value="(なし)" onclick="applyTemplate(\'\');" style="background-color:#FCC" /><br />';
        for ( var i=0; i<speakers.length; i++ ) {
            if ( !speakers[i] ) break;
            speaker_html += '<input type="button" value="'+w.esc(speakers[i])+'" onclick="applyTemplate(\''+w.esc(speakers[i])+'\');" /><br />';
        }
        document.getElementById('ttm_menu').style.display = 'none';
        
        //console.log(w.esc(w.getCookie('ttm_template')));
        //console.log(w.esc(w.getCookie('ttm_speakers')));
        document.getElementById('ttm_template').value = w.esc(w.getCookie('ttm_template'));
        document.getElementById('ttm_speakers').value = w.esc(w.getCookie('ttm_speakers'));
        //document.getElementById('ttm_template_view').innerHTML = w.esc(w.getCookie('ttm_template'));
        document.getElementById('ttm_speakers_view').innerHTML = speaker_html;
        document.getElementById('ttm_setting').href = 'javascript:showSettings();';
    }

    /**
     * escape HTML specialchars
     */
    w.esc = function(str) {
        str = str.replace(/&/g,'&amp;');   // &
        str = str.replace(/</g,'&lt;');    // <
        str = str.replace(/>/g,'&gt;');    // >
        str = str.replace(/\"/g,'&quot;'); // "
        str = str.replace(/\'/g,'&#039;'); // '
        return str;;
    }

    w.getPositions = function(obj) {
        var menuW = obj.offsetWidth;
        var menuH = obj.offsetHeight;
        var menuT = 0, menuL = 0;
        do {
            menuT += obj.offsetTop  || 0;
            menuL += obj.offsetLeft || 0;
            obj   =  obj.offsetParent;
        } while (obj);
        //w.console.log(menuW+'/'+menuH+'/'+menuT+'/'+menuL);
        return {w:menuW,h:menuH,t:menuT,l:menuL};
    }

    /**
     * init
     */
    function init() {
        ttm_a = document.createElement('a');
        ttm_a.id   = 'ttm_setting';
        ttm_a.href = 'javascript:showSettings();';
        ttm_a.innerHTML = 'TweetTheMinutes';

        ttm_li = document.createElement('li');
        ttm_li.appendChild(ttm_a);
        
        document.getElementById('sign_out_link').parentNode.parentNode.insertBefore(
            ttm_li,
            document.getElementById('sign_out_link').parentNode
        );
        
        // menu position
        var pos = w.getPositions(ttm_li);
        //console.log(pos);

        var div = document.createElement('div');
        div.setAttribute('id','ttm_menu');
        div.style.display = 'none';
        div.style.margin = 0;
        div.style.padding = '7px';
        div.style.background = '#FFF';
        div.style.borderRight = '1px solid #999';
        div.style.borderLeft = '1px solid #999';
        div.style.borderBottom = '1px solid #999';
        div.style.position = 'absolute';
        div.style.top    = (pos.t+pos.h)+'px';
        div.style.left   = (pos.l+pos.w-320)+'px';
        div.style.zIndex = '99';
        div.innerHTML
            = '<p style="text-align:left;padding:5px">'
            + '<label for="ttm_template">Template:</label><br />'
            + '<input type="text" id="ttm_template" name="ttm_template" value="" style="width:300px;font:120%/120% sans-serif;" /><br />'
            + '<small>テンプレートを定義します。<br />'
            + '<b>{$speaker}</b> は選択したSpeakerに変換されます。<br />'
            + '<b>{$status}</b> はすでにツイート欄に入っているテキストが入ります</small>'
            + '</p>'
            + '<p style="text-align:left;padding:5px">'
            + '<label for="ttm_speakers">Speakers:</label><br />'
            + '<input type="text" id="ttm_speakers" name="ttm_speakers" value="" style="width:300px;font:120%/120% sans-serif;" /><br />'
            + '<small>発言者を半角スペース区切りで複数登録できます</small>'
            + '</p>'
            + '<p style="text-align:center;padding:5px">'
            + '<input type="button" value="save" onclick="saveSettings();" />'
            + '</p>';
        document.body.appendChild(div);
        
        // textarea position
        pos = w.getPositions(document.getElementById('status'));
        //console.log(pos);

        div = document.createElement('div');
        div.setAttribute('id','ttm_block');
        div.className = 'profile-controls round';
        div.style.width     = '100px';
        div.style.position  = 'absolute';
        div.style.top       = (pos.t-20)+'px';
        div.style.left      = (pos.l-130)+'px';
        div.style.zIndex    = '99';
        div.innerHTML
            = '<label for="ttm_speakers">Speakers:</label><br />'
            + '<div id="ttm_speakers_view" style="height:150px;overflow:auto;"></div>';
        document.body.appendChild(div);
        w.hideSettings();
    }

    /**
     * Crossbrowser addEventListener
     */
    function _AddEventListener(e, type, fn) {
        if (e.addEventListener) {
            e.addEventListener(type, fn, false);
        }
        else {
            e.attachEvent('on' + type, function() {
                fn(window.event);
            });
        }
    }

    if ( document.getElementById('status') ) {
        init();
    }

})();